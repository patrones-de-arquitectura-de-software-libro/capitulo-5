import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

import {SPCLocalize} from '../components/spc-localize.js';

class IndexView extends SPCLocalize {
  static get template() {
    return html`
      <style include="global">
        :host {
        }
        
        paper-input {
          margin: 10px 0px;
          --paper-input-container-focus-color: var(--paper-red-900);
          --paper-input-container-label-focus-color: var(--paper-red-900);
          
          --paper-input-container-label: {
            color: var(--paper-red-900);
          };
          
          --paper-input-container: { 
            padding: 10px;
            background: var(--paper-grey-50);
            border-radius: 5px;
          };
          
          --paper-input-container-underline: { 
            background: var(--paper-red-900);
          };
          
          --paper-input-container-underline-focus: { 
            background: var(--paper-red-900);
          };
          
          --paper-input-container-input: {
            color: var(--paper-grey-900);
          };
          
          --paper-input-container-label-floating: {
            color: var(--paper-red-900);
          };
          
          --paper-input-container-input-invalid: {
            color: var(--paper-red-900);
          };
        }
      </style>

      <div class="header">
      
        <span class="main-title">
          Netflix Inc.
        </span>
        
        <paper-input
          id="title"
          label="{{ localize('MOVIE_INPUT', 'type', 'title') }}"
          placeholder="{{ localize('EXAMPLE_MOVIE', 'type', 'title') }}"
          value="{{title}}"
          required>
        </paper-input>
        
        <paper-input
          id="username"
          label="{{ localize('TWITTER_INPUT', 'type', 'title') }}"
          placeholder="{{ localize('EXAMPLE_TWITTER_USER', 'type', 'title') }}"
          value="{{username}}"
          required>
        </paper-input>
        
        <paper-button
          on-tap="_performAnalysis">
          {{ localize('PERFORM_BUTTON', 'type', 'title') }}
        </paper-button>
        
        <div class="max-w">
          <span class="main-description main-wrap">
            {{ localize('MAIN_DESCRIPTION', 'type', 'title') }}
          </span>
        </div>
      </div>
    `;
  }
  
  static get properties() {
    return {
      title: {
        type: String,
        value: ''
      },
      username: {
        type: String,
        value: ''
      }
    };
  }
  
  _performAnalysis(){
    if(this.$.title.validate() && this.$.username.validate()){
      window.history.pushState({}, null, this.rootPath + 'analysis?title=' + this.title + '&username=' + this.username);
      window.dispatchEvent(new CustomEvent('location-changed'));
    }
  }
}

window.customElements.define('index-view', IndexView);