import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icons/social-icons.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icon/iron-icon.js';

import {SPCLocalize} from '../components/spc-localize.js';
import '../components/spc-connector.js';
import '../components/spc-chip.js';
import '../components/spc-loading.js';

class AnalysisView extends SPCLocalize {
  static get template() {
    return html`
      <style include="global">
        :host {
          display: block;
           --iron-icon-height: 80px;
           --iron-icon-width: 80px;
           --iron-icon-fill-color: var(--paper-red-900);
        }
      </style>
      
      <div class="horizontal center">
        <span class="analysis-title">
          {{ localize('RESULT_ANALYSIS', 'type', 'title') }}
        </span>
      </div>
      
      <div class="horizontal">
      
        <div class="vertical">
          <div class="card">
            
            <div class="horizontal center">
              <span class="title">
                {{ localize('SUMMARY', 'type', 'title') }}
              </span>
            </div>
            
            <div class="horizontal center">
              <img
                class="poster"
                src="{{image}}">
              </img>
            </div>
            
            <div class="horizontal center">
              <span class="movie-title">[[movieTitle]]</span>
            </div>
            
            <div class="horizontal">
              <div class="vertical flex">
                <div class="horizontal center">
                  <span class="movie-details">[[movieYear]]</span>
                </div>
              </div>
              <div class="vertical">
                <div class="horizontal center">
                  <span class="movie-details">·</span>
                </div>
              </div>
              <div class="vertical flex">
                <div class="horizontal center">
                  <span class="movie-details">[[movieRuntime]]</span>
                </div>
              </div>
            </div>
            
            <div class="horizontal center">
              <a class="movie-website" href="[[movieWebsite]]">website</a>
            </div>
            
          </div>
        </div>
        
        <div class="vertical flex">
        
          <div class="card">
            <div class="horizontal start">
              <span class="title">{{ localize('DETAILS', 'type', 'title') }}</span>
            </div>
            <div class="horizontal">
              <span class="movie-details">[[moviePlot]]</span>
            </div>
            <div class="horizontal">
              <div class="vertical flex">
                <div class="horizontal center">
                  <span class="movie-title">{{ localize('ACTORS', 'type', 'title') }}</span>
                </div>
                <div class="horizontal wrap center">
                  <template is="dom-repeat" items="{{movieActors}}">
                    <scp-chip label="[[item]]"></scp-chip>
                  </template>
                </div>
              </div>
              <div class="vertical flex">
                <div class="horizontal center">
                  <span class="movie-title">{{ localize('GENRES', 'type', 'title') }}</span>
                </div>
                <div class="horizontal wrap center">
                  <template is="dom-repeat" items="{{movieGenre}}">
                    <scp-chip label="[[item]]"></scp-chip>
                  </template>
                </div>
              </div>
            </div>
            
            <div class="horizontal center">
              <span class="movie-title">{{ localize('DIRECTORS', 'type', 'title') }}</span>
            </div>
            
            <div class="horizontal center">
              <div class="horizontal wrap center">
                  <template is="dom-repeat" items="{{movieDirector}}">
                    <scp-chip label="[[item]]"></scp-chip>
                  </template>
                </div>
            </div>
            
          </div>
          
          <div class="card">
            <div class="horizontal start">
              <span class="title">{{ localize('RATINGS', 'type', 'title') }}</span>
            </div>
            <div class="horizontal justified">
              <template is="dom-repeat" items="{{movieRatings}}">
                <div class="vertical flex">
                  <div class="horizontal center">
                    <span class="movie-rating">[[item.Value]]</span>
                  </div>
                  <div class="horizontal center">
                    <span class="movie-details">[[item.Source]]</span>
                  </div>
                </div>
              </template>
            </div>
          </div>
          
          <div class="card">
            <div class="horizontal start">
              <span class="title">{{ localize('LAST_COMMENTS', 'type', 'title') }}</span>
            </div>
            <div class="horizontal">
              <div class="vertical flex">
                <div class="horizontal center">
                  <iron-icon icon="social:sentiment-satisfied">
                  </iron-icon>
                </div>
                <div class="horizontal center">
                  <span class="movie-rating">[[positive]]</span>
                </div>
                <div class="horizontal center">
                  <span class="movie-details">{{ localize('POSITIVE', 'type', 'title') }}</span>
                </div>
              </div>
              <div class="vertical flex">
                <div class="horizontal center">
                  <iron-icon icon="social:sentiment-neutral">
                  </iron-icon>
                </div>
                <div class="horizontal center">
                  <span class="movie-rating">[[neutral]]</span>
                </div>
                <div class="horizontal center">
                  <span class="movie-details">{{ localize('NEUTRAL', 'type', 'title') }}</span>
                </div>
              </div>
              <div class="vertical flex">
                <div class="horizontal center">
                  <iron-icon icon="social:sentiment-dissatisfied">
                  </iron-icon>
                </div>
                <div class="horizontal center">
                  <span class="movie-rating">[[negative]]</span>
                </div>
                <div class="horizontal center">
                  <span class="movie-details">{{ localize('NEGATIVE', 'type', 'title') }}</span>
                </div>
              </div>
              <div class="vertical flex">
                <div class="horizontal center">
                  <iron-icon icon="icons:search">
                  </iron-icon>
                </div>
                <div class="horizontal center">
                  <span class="movie-rating">[[total]]</span>
                </div>
                <div class="horizontal center">
                  <span class="movie-details">{{ localize('TOTAL_REVIEWS', 'type', 'title') }}</span>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
      
      <div class="horizontal center">
        <paper-button on-tap="_backIndex">
          {{ localize('PERFORM_AGAIN', 'type', 'title') }}
        </paper-button>
      </div>
      
      <div class="horizontal m-bottom">
        <div class="vertical flex">
          <div class="horizontal center">
            <span class="title">{{ localize('MICROSERVICES_PATTERN', 'type', 'title') }}</span>
          </div>
          <div class="horizontal center">
            <span class="movie-title">{{ localize('SOFTWARE_ARCHITECTURE', 'type', 'title') }}</span>
          </div>
          <div class="horizontal center">
            <span class="movie-details">Perla Velasco Elizondo & Yonathan A. Martínez Padilla</span>
          </div>
          <div class="horizontal center">
            <span class="movie-rights">Copyright &copy; {{ localize('UNIVERSITY', 'type', 'title') }} {{_getCurrentYear()}}</span>
          </div>
        </div>
      </div>
      
      <scp-connector
        id="connect">
      </scp-connector>
      <spc-loading id="loading" time="40000"></spc-loading>
    `;
  }
  
  ready() {
    super.ready();
    this.$.connect.addEventListener('done', e => this._handleResponse(e));
  }
  
  static get properties() {
    return {
      t: {
        type: String,
        value: ''
      },
      username: {
        type: String,
        value: ''
      },
      image: {
        type: String,
        value: ''
      },
      movieTitle: {
        type: String,
        value: ''
      },
      movieYear: {
        type: String,
        value: ''
      },
      movieRuntime: {
        type: String,
        value: ''
      },
      movieWebsite: {
        type: String,
        value: ''
      },
      moviePlot: {
        type: String,
        value: ''
      },
      movieActors: {
        type: Array,
        value: []
      },
      movieGenre: {
        type: Array,
        value: []
      },
      movieDirector: {
        type: Array,
        value: []
      },
      movieRatings: {
        type: Array,
        value: []
      },
      negative: {
        type: String,
        value: ''
      },
      positive: {
        type: String,
        value: ''
      },
      neutral: {
        type: String,
        value: ''
      },
      total: {
        type: String,
        value: ''
      }
    };
  }
  
  static get observers() {
    return [
      '_titleChange(t)'
    ];
  }
  
  _titleChange(title){
    if(title !== undefined && title !== ''){
      this.$.connect.getMovieInformation(title);
      this._showLoader('LOADING_MOVIE_INFORMATION');
    }
  }
  
  _backIndex(){
    window.history.pushState({}, null, this.rootPath + 'index');
    window.dispatchEvent(new CustomEvent('location-changed'));
  }
  
  _handleResponse(e){
    this.$.loading.close();
    let query = e.detail.query;
    let data = e.detail.data;
    switch(query){
      case 'getMovieInformation':
        this._loadBasicDataMovie(data);
        break;
      case 'getTweets':
        this._analyzeTweets(data);
        break;
      case 'performAnalysis':
        this._loadAnalysisResult(data);
        break;
    }
  }
  
  _loadBasicDataMovie(movie){
    if(movie !== undefined && movie !== null){
      this.image = movie['Poster'];
      this.movieTitle = movie['Title'];
      this.movieRuntime = movie['Runtime'];
      this.movieYear = movie['Year'];
      this.movieWebsite = movie['Website'];
      this.moviePlot = movie['Plot'];
      this.movieActors = movie['Actors'].split(",");
      this.movieGenre = movie['Genre'].split(",");
      this.movieDirector = movie['Director'].split(",");
      this.movieRatings = movie['Ratings'];
      this.movieRatings.push({'Source': 'IMDB', 'Value': movie['imdbRating']});
      this.movieRatings.push({'Source': 'Metascore', 'Value': movie['Metascore']});
      this.$.connect.getTweets(this.username.replace('@', ''));
      this._showLoader('RETRIEVING_MOVIE_TWEETS');
    }
  }
  
  _analyzeTweets(data){
    let tweets = [];
    for (let i = 0; i < data.length; i++) {
      let tweet = { text: data[i]};
      tweets.push(tweet);
    }
    this.$.connect.performAnalysis(tweets);
    this._showLoader('ANALYZING_TWEETS');
  }
  
  _loadAnalysisResult(result){
    this.negative = result['negative'];
    this.positive = result['positive'];
    this.neutral = result['neutral'];
    this.total = result['total reviews'];
  }
  
  _getCurrentYear(){
    return new Date().getFullYear();
  }

  _showLoader(message){
    this.async(function(){
      this.$.loading.lbText = this.localize(message, 'type', 'title');
      this.$.loading.open();
    }, 100);
  }
}

window.customElements.define('analysis-view', AnalysisView);