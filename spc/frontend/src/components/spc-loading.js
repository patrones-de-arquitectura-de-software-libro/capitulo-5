import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-spinner/paper-spinner.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';

class SPCLoading extends PolymerElement {
  static get template() {
    return html`
      <style include="dialog">
        paper-spinner.multi {
          --paper-spinner-layer-1-color: var(--paper-red-900);
          --paper-spinner-layer-2-color: var(--paper-red-900);
          --paper-spinner-layer-3-color: var(--paper-red-900);
          --paper-spinner-layer-4-color: var(--paper-red-900);
        }

        .horizontal {
        	@apply --layout-horizontal;
        }

        .center {
        	@apply --layout-center-justified;
        }

        .vertical {
        	@apply --layout-vertical;
        }

        .end {
        	@apply --layout-end-justified;
        }

        .margin-10 {
        	margin: 10px;
        }

        .description {
        	@apply --paper-font-common-base;
        	@apply --paper-font-common-nowrap;
        	font-size: 20px;
        	font-weight: 400;
        	line-height: 28px;
        	color: var(--paper-grey-900);
        }

        paper-dialog {
        	padding: 15px;
        	border-radius: 10px;
        }

        .back {
        	background: rgba(0, 0, 0, 0.5);;
        	transform: translateZ(0);
        	position: fixed;
        	width: 100%;
        	z-index: 80;
        	height: 100%;
        	top: 0;
        	left: 0;
        }

        .no-visible {
        	display: none;
        }
      </style>
      <div class$="back {{clazz}}">
      </div>
      
      <paper-dialog id="dialog" no-cancel-on-outside-click no-cancel-on-esc-key>
        <template is="dom-if" if="[[show]]">
          <div class="horizontal end">
            <paper-icon-button
              on-tap="close"
              icon="icons:close">
            </paper-icon-button>
          </div>
        </template>
        <div class="horizontal center">
          <div class="vertical center">
            <div class="horizontal center">
              <paper-spinner class="multi" active></paper-spinner>
            </div>
            <div class="horizontal center">
              <span class="description margin-10">[[lbText]]</span>
            </div>
          </div>
        </div>
      </paper-dialog>
    `;
  }
  
  static get properties() {
    return {
      lbText: {
        type: String,
        value: 'Loading...'
      },
      show: {
        type: Boolean,
        value: false
      },
      time: {
        type: Number,
        value: 1000
      },
      clazz: {
        type: String,
        value: "no-visible"
      }
    };
  }
  
  open(){
    this.clazz = "";
    this.$.dialog.open();
    if(this.time !== -1){
      let timeOut = setTimeout(this._showCloseIcon.bind(this), this.time);
    }
  }
  
  close(){
    this.clazz = "no-visible";
    this.$.dialog.close();
    this.show = false;
  }
  
  _showCloseIcon(){
    this.show = true;
  }
}

window.customElements.define('spc-loading', SPCLoading);