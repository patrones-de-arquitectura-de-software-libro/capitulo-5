/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement} from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import {Polymer} from '@polymer/polymer/lib/legacy/polymer-fn.js';
import {html} from '@polymer/polymer/lib/utils/html-tag.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';

import './styles/global.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class SPCApp extends PolymerElement {
  static get template() {
    return html`
      <style include="global">
        :host {
          --app-primary-color: var(--paper-red-900);
          --app-secondary-color: var(--paper-grey-50);

          display: block;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]" query-params="{{params}}">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>
      
      <app-header-layout>
        <!--
        <app-header reveals effects="waterfall" slot="header">
          <app-toolbar class="toolbar">
            <div class="tabs">
              <a href="#about">Inicio</a>
            </div>
          </app-toolbar>
        </app-header>
        -->
        
        <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
        
          <index-view
            name="index">
          </index-view>
          
          <analysis-view 
            name="analysis"
            t="{{title}}"
            username="{{username}}">
          </analysis-view>
          
        </iron-pages>
      </app-header-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        value: 'index',
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      params: {
        type: Object,
        observer: '_changeParams'
      },
      title: {
        type: String,
        value: ''
      },
      username: {
        type: String,
        value: ''
      }
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    if (page !== "" && page !== undefined && page !== null) {
      this.page = page;
    }
  }

  _pageChanged(page) {
    switch (page) {
      case 'index':
        import('./pages/index.js');
        break;
      case 'analysis':
        import('./pages/analysis.js');
        break;
    }
  }
  
  _changeParams(value){
    if(value.title !== undefined){
      this.title = value.title;
      this.username = value.username;
    } else {
      this.title = 'Stranger things';
      this.username = '@Stranger_things';
    }
  }
}

window.customElements.define('spc-app', SPCApp);
