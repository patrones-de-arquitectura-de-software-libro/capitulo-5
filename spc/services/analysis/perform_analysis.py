from flask import Flask, request
import urllib
import json


def perform_analysis(request):
    # Método que análiza el sentimiento expresado en un texto en particular
    # Se definen e inicializan las variables que contabilizarán los sentimientos 
    positive = 0
    negative = 0
    neutral = 0
    total_reviews = 0
    # Se define e inicializa un objeto JSON que contendrá la respuesta
    json_data = {}
    # Se lee y convierte en JSON el contenido recibido a través del método POST
    input_json = request.get_json()
    # Se itera cada texto contenido en el JSON para su análisis de sentimientos
    for r in input_json:
        # Se codifica un JSON que será enviado al API de text-processing para analizar el sentimiento
        data = urllib.parse.urlencode({"text": r['text']}).encode("utf-8")
        # Se conecta con el API de text-processing y se envía el JSON codificado
        u = urllib.request.urlopen("http://text-processing.com/api/sentiment/", data=data)
        # Se lee el resultado del análisis
        result = u.read()
        # Se convierte en JSON el resultado leído para su contabilizar los sentimientos
        json_result = json.loads(result)
        # Se contabiliza el sentimiento de acuerdo al tipo de sentimiento
        if json_result['label'] == 'pos':
            positive += 1
        elif json_result['label'] == 'neg':
            negative += 1
        elif json_result['label'] == 'neutral':
            neutral += 1
    # Se llena el JSON que contendrá la respuesta del análisis de sentimientos
    json_data['positive'] = str(positive)
    json_data['negative'] = str(negative)
    json_data['neutral'] = str(neutral)
    total_reviews = positive + negative + neutral
    json_data['total reviews'] = str(total_reviews)
    # Se devuelve el JSON que contiene la repsuesta
    return json.dumps(json_data)
